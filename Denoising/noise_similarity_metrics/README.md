# Noise Similarity Metrics

This readme file provides the steps to run the noise similarty metrics. 
The metrics for measuring the quality of the (reconstructed noise vs input noise) are: Kullback-Leibler (KL) divergence, Jensen–Shannon (JS) divergence – symmetric KL, Kolmogorov-Smirnov (KS) statistic, Wasserstein distance, and the metrics for measuring the quality of (reconstructed noisy image vs input noisy image) are PSNR and SSIM.

The base of the this code is taken from the code available at: https://gitlab.com/wg1/jpeg-ai/jpeg-ai-qaf
Note:
The reconstructed noise is obtained as: Reconstrusted_noisy_input - Reconstructed_clean_input
The noise in the input is obtained as: Noisy_input - clean_input
This means that we need the original clean, original noisy, reconstructed clean, reconstrusted noisy to compute the metrics. The Reconstructed_clean_input is obtained by passing the clean_input instead of the noisy input to the codec. You don't need to match the bitrate for the encoded clean image and the encoded noisy images. Just use the same model you used for noisy images to encode the clean images.  

## To set up:
1) ``cd noise_similairty_metrics``
2) create conda environment: ``conda create -n test_generator python=3.6.9``
3) activate environment: ``conda activate test_generator``
4) install dependencies: ``pip install -r requirements.txt``


## 2) To run:
First encode and decode the noisy images and gather them in CODEC directory (without any subfolders) by storing the encoded bitstreams in  ``bit`` and all the decoded images in ``rec`` folder. For each test image, there may be multiple noisy images. For each set of noisy images there will be a seperate report. Also encode and decode the clean images with the same CODEC and put the reconstructed files in the corresponding folder as show below. 


### Structure of directories with results

Prepare the following direcotries:

    Original clean images
        00013_TE_800x1200_8bit_sRGB.png
        …

    Reconstructed clean images
        00013_TE_800x1200_8bit_sRGB_<BR>.png
        …
    Original noisy images
        00013_<NOISE LEVEL>_TE_800x1200_8bit_sRGB.png
        …
    SUBMISSIONS
        CODEC
            organize_noise_folders.sh
            bit
                <TEAMID>_<IMGID>_<NOISE LEVEL>_TE_<BR>.bits
                …
            rec
                <TEAMID>_<IMGID>_<NOISE LEVEL>_TE_<Width>x<Height>_<ORIGINAL BIT DEPTH>bit_sRGB_<BR>.png
                …

Where ``CODEC`` is the codec name. ``bit`` and ``rec`` are the directories with bitstreams and reconstructed files. ``organize_noise_folders.sh`` is the script that we will call to get the final results. Copy  ``organize_noise_folders.sh`` from noise_similairty_metrics folder to the ``CODEC`` folder.

In ``organize_noise_folders.sh``  file you should setup the following variables before running it:

main_dir="."  ==> This is set to the inside of the CODEC folder

bits_extension="tfci"  ==> What the extension of the bit files are 

evaluation_code_dir="somedir" ==> Where the noise similarity metrics code main_noise_report.py is located

original_testset="somedir" ==> Where JPEG-testset (clean images) is located

noisy_testset="somedir" ==> Where Noisy dataset (original noisy images) is located

base_folder="somedir" ==> Where the CODEC folder is located

clean_dec="someidr" ==> Where the reconstructed clean images are

codec=codec_name ==> codec name

Then cd to where CODEC is located and run 
 ``chmod +x ./organize_noise_folders.sh``
 
 ``./organize_noise_folders.sh``

The code will generate 10 csv report file in the CODEC dir for 10 different versions of the noisy images. 
<!--- 
### Iterate over all images

Follow these steps in your local copy of the scripts:

To get the evaluation results run:

 ``python main.py <PATH_TO_ORIGs> <PATH_TO_BASE_DIR> <CODEC_NAME> <PATH_TO_ORIGs_NOISY>``, where `<PATH_TO_ORIGs>` is a path to original clean files, `<PATH_TO_BASE_DIR>` is a path to base directory with results (`SUBMISSIONS` in a section `Structure of directories with results`), `<CODEC_NAME>` is a name of the codec under test and is `<PATH_TO_ORIGs_NOISY>` the path to the Noisy original files. 

By default all supported metrics are performed for the input image as one vector. In addition, the metrics for each channel are provided. 

Format of output summary is tab-separated text file. CSV format is also supported. To have output file in CSV format add `--csv` to command line.

if you need to calculate only distortion metrics without BPP calculation, use command line parameter ``--no-bpp``. It will fill corresponding column by zeros.


### Process of particular image

Processing of particular image is also possible. For this purpose you may use a script ``calc_metric.py``.
Typical usage is a following:

``python calc_metric.py PATH_TO_ORIG PATH_TO_REC``

where `PATH_TO_ORIG` is a path to a original file, `PATH_TO_REC` is a path to a reconstructed file.

To add BPP calculation to an output use option `--bit` with a path to binary file:

``python calc_metric.py PATH_TO_ORIG PATH_TO_REC --bit PATH_TO_BIT``

where `PATH_TO_BIT` is a path to bit file.

The script is also allow to add some additional data to the output. Use option `--add` and list values which should be added:

``python calc_metric.py PATH_TO_ORIG PATH_TO_REC --add OPTION1 OPTION2``

output file will contain the following line:

``REC_FILE_NAME METRIC1 METRIC2 ... OPTION1 OPTION2``


By default, the script will store data to a file `summary.txt`. If you prefare to store it somewhere else, add option `--summary` and add a name of the output file. Data will be added to file instead of rewriting the file.

If you prefare to parse an output instead of file, add option `--only-print` and the script will print results without writing them to an output file. The output will be like:
``
Results: REC_FILE_NAME  METRIC1 METRIC2 ...
``

where columns `METRIC..` separated by tab.
--->


### Naming format for input files:
#### Original clean images
`<Name>_TE_<Width>x<Height>_<ORIGINAL BIT DEPTH>bit_sRGB.png`

where `<Name>` is a name of the image file, `<Width>` and `<Height>` are width and height of the image, and `<ORIGINAL BIT DEPTH>` is the bit depth

Example of the naming:
`00013_TE_800x1200_8bit_sRGB.png`

#### Original reconstructed images
`<Name>_TE_<Width>x<Height>_<ORIGINAL BIT DEPTH>bit_sRGB_<BR>.png`

where `<Name>` is a name of the image file, `<Width>` and `<Height>` are width and height of the image, and `<ORIGINAL BIT DEPTH>` is the bit depth
and <BR> is the bitrate
Example of the naming:
`00013_TE_800x1200_8bit_sRGB_012.png`

#### Original noisy images
`<Name>_<NOISE ID>_TE_<Width>x<Height>_<ORIGINAL BIT DEPTH>bit_sRGB.png`
where `<Name>` is a name of the image file and `<NOISE ID>` is the indicator of noise level

Example of the naming:
'00013_<001>_TE_800x1200_8bit_sRGB.png'

#### Reconstrusted noisy images

` <TEAMID>_<IMGID>_<NOISE LEVEL>_TE_<Width>x<Height>_<ORIGINAL BIT DEPTH>bit_sRGB_<BR>.png`

where `<TEAMID>` is a name of the codec, `<BR>` is the bitrate.

Example of the naming:
`HYPER_00015_003_TE_976x1472_8bit_sRGB_012.png`

#### Bitrstream files
`  <TEAMID>_<IMGID>_<NOISE LEVEL>_TE_<BR>.bits`


Example of the naming:
`HYPER_00002_001_TE_012.bits`


