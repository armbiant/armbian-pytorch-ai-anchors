# Denoising JPEG-AI

The repository for image denoising task in JPEG-AI framework. 

This repository has three sections:

1) [Noise Generator](noise_generator/README.md): Code for the practical noise generator 

2) [Anchors](anchors/README.md):  Codes to obtain the anchor results for the denoising task 

3) [Noise similarity metircs](noise_similarity_metrics/README.md):  Codes to get noise similarity metrics for evaluating the quality of the reconstructed noise 

For each section readme files including the instructions to run the codes are provided.

