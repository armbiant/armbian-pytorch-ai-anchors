def get_models():
    from Classification import get_models as get_models_classification
    from Compression import get_models as get_models_compression
    
    return get_models_classification() + get_models_compression()

if __name__ == "__main__":
    from common.Base import make_main_func
    
    make_main_func(get_models())