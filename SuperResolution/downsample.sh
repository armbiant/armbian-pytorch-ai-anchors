#!/usr/bin/env bash

input_dir="./data/original_images"

output1_dir="./data/down4xbicubic"
output2_dir="./data/down4xlanczos3"

mkdir -p $output1_dir
mkdir -p $output2_dir

input_list=$(ls -1 $input_dir|grep .png)

for input in $input_list
do
    ffmpeg -hide_banner \
        -i $input_dir/$input \
        -vf scale=w=iw/4:h=ih/4:in_range=full:out_range=full \
        -sws_flags bicubic+print_info+accurate_rnd+bitexact \
        -sws_dither none \
        -y $output1_dir/$input
done
wait

for input in $input_list
do
    ffmpeg -hide_banner \
        -i $input_dir/$input \
        -vf scale=w=iw/4:h=ih/4:in_range=full:out_range=full \
        -sws_flags lanczos+print_info+accurate_rnd+bitexact \
        -param0 3 \
        -sws_dither none \
        -y $output2_dir/$input
    # '-param0 3': sets the width of the Lanczos filter
done
wait
