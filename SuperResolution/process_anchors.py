import os
import glob
from PIL import Image
import numpy as np

from model.wdsr import wdsr_b
from model import resolve_single
from utils import load_image

# Number of residual blocks
depth = 32
# Super-resolution factor
scale = 4

# Location of model weights
weights_dir = f'weights/wdsr-b-{depth}-x{scale}'
weights_file = os.path.join(weights_dir, 'weights.h5')
print(weights_file)

model = wdsr_b(scale=scale, num_res_blocks=depth)
model.load_weights(weights_file)


data_dir='../data'

input_dirs=['down4xbicubic','down4xlanczos3']


for input_dir in input_dirs:
    os.makedirs(f'{data_dir}/output/{input_dir}-up4xwdsrb32/rec/', exist_ok=True)
    inputs = sorted([i.split('/')[-1] for i in glob.glob(f'{data_dir}/{input_dir}/*.png')])
    for input_file in inputs:
        output_file=f'{input_dir}-up4xwdsrb32_{input_file[:-4]}_8bit_sRGB_000.png'
        print(f'{input_file} --> {output_file}')
        lr = load_image(f'{data_dir}/{input_dir}/{input_file}')
        sr = resolve_single(model, lr)
        img = Image.fromarray(np.array(sr))
        img.save(f'{data_dir}/output/{input_dir}-up4xwdsrb32/rec/{output_file}')
