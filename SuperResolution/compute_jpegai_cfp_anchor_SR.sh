#!/usr/bin/env bash

codecs="down4xbicubic-up4xbicubic
down4xbicubic-up4xlanczos3
down4xbicubic-up4xlanczos8
down4xbicubic-up4xwdsrb32
down4xlanczos3-up4xbicubic
down4xlanczos3-up4xlanczos3
down4xlanczos3-up4xlanczos8
down4xlanczos3-up4xwdsrb32"


for codec in $codecs
do
    python main.py -v --rates 0 --csv --no-bpp \
        ../SuperResolution/data/original_images/ \
        ../SuperResolution/data/output/ \
        $codec &
done
wait