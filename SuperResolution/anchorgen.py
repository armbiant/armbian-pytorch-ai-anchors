#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Anchor generation


import argparse
import os
import subprocess
from PIL import Image


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('--config', default="config.json", help="Load configuration file")
    ap.add_argument("--force-regenerate", default=False, action="store_true", help="Regenerate everything")

    args = ap.parse_args()

    # Read the config

    # check_prerequisites()
    check_input('data/original_images/00001_TE_2096x1400.png')

    
def init_dirs():
    pass

def check_prerequisites():
    p = subprocess.run(["ffmpeg","-version"],capture_output=True)

def check_input(input_file,factor=4):
    im = Image.open(input_file)
    width, height = im.size
    if ((width%factor!=0)|(height%factor!=0)):
        raise SystemExit(f'ERROR: Input is not factor of {factor}')



if __name__ == "__main__":
    main()
