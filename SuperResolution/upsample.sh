#!/usr/bin/env bash

input1_dir="./data/down4xbicubic"
input2_dir="./data/down4xlanczos3"
input_dirs="$input1_dir $input2_dir"

output_dir="./data/output"
mkdir -p $output_dir

# Iterate over two different input-downsample methods 
for input_dir in $input_dirs
do
    input_list=$(ls -1 $input_dir|grep .png)

    # Bicubic upsampling
    codec=$(basename $input_dir)-up4xbicubic
    mkdir -p $output_dir/$codec
    mkdir -p $output_dir/$codec/rec
    mkdir -p  $output_dir/$codec/bit
    for input in $input_list
    do
        # output_file=${codec}_${input:16}
        output_file=${codec}_${input:0:-4}_8bit_sRGB_000.png
        ffmpeg -hide_banner \
            -i $input_dir/$input \
            -vf scale=w=iw*4:h=ih*4:in_range=full:out_range=full \
            -sws_flags bicubic+print_info+accurate_rnd+bitexact \
            -sws_dither none \
            -y $output_dir/$codec/rec/$output_file &
        # cp $input_dir/bit/${input:0:24}${input:(-8):4}.bits $output_dir/$codec/bit/${output_file:0:41}${output_file:(-8):4}.bits &
    done
    wait

    # Lanczos3 upsampling
    codec=$(basename $input_dir)-up4xlanczos3
    mkdir -p $output_dir/$codec
    mkdir -p $output_dir/$codec/rec
    mkdir -p  $output_dir/$codec/bit
    for input in $input_list
    do
        # output_file=${codec}_${input:16}
        output_file=${codec}_${input:0:-4}_8bit_sRGB_000.png
        ffmpeg -hide_banner \
            -i $input_dir/$input \
            -vf scale=w=iw*4:h=ih*4:in_range=full:out_range=full \
            -sws_flags lanczos+print_info+accurate_rnd+bitexact \
            -param0 3 \
            -sws_dither none \
            -y $output_dir/$codec/rec/$output_file &
        # cp $input_dir/bit/${input:0:24}${input:(-8):4}.bits $output_dir/$codec/bit/${output_file:0:42}${output_file:(-8):4}.bits &
        # '-param0 3': sets the width of the Lanczos filter
    done
    wait

    # Lanczos8 upsampling
    codec=$(basename $input_dir)-up4xlanczos8
    mkdir -p $output_dir/$codec
    mkdir -p $output_dir/$codec/rec
    mkdir -p  $output_dir/$codec/bit
    for input in $input_list
    do
        # output_file=${codec}_${input:16}
        output_file=${codec}_${input:0:-4}_8bit_sRGB_000.png
        ffmpeg -hide_banner \
            -i $input_dir/$input \
            -vf scale=w=iw*4:h=ih*4:in_range=full:out_range=full \
            -sws_flags lanczos+print_info+accurate_rnd+bitexact \
            -param0 8 \
            -sws_dither none \
            -y $output_dir/$codec/rec/$output_file &
        # cp $input_dir/bit/${input:0:24}${input:(-8):4}.bits $output_dir/$codec/bit/${output_file:0:42}${output_file:(-8):4}.bits &
        # '-param0 8': sets the width of the Lanczos filter
    done
    wait
done

