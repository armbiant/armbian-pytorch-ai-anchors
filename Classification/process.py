import argparse
import re
import numpy as np
import os
import torch
import torchvision.models as models
import torchvision.transforms as transforms
import torchvision.datasets as datasets

class CustomFolder(datasets.ImageFolder):
    """
    Customed Image Folder
    """
    def __init__(
            self,
            root,
            transform = None,
            is_valid_file = None
            ):
        super(CustomFolder, self).__init__(root, transform=transform, is_valid_file=is_valid_file)
        self.idx_to_class = {i: cls_name for cls_name, i in self.class_to_idx.items()}

    def __getitem__(self, index):
        path, target = self.samples[index]
        sample = self.loader(path)
        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            target = self.target_transform(target)
        return sample, target, path


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

from common.Base import Base

class Classification(Base):
    def __init__(self, *args, **kwards):
        super(Classification, self).__init__("Classification", *args, **kwards)
        self._dataset_path = os.path.join(os.getcwd(), "dataset_classification")
        self.top1 = AverageMeter()
        self.top5 = AverageMeter()
        self.total_decCPU_time_in_sec = 0.0
        self.total_decGPU_time_in_sec = 0.0
        self.total_decMACs = 0.0
        self.total_time_in_sec = 0.0
        self.total_flops = 0
        self.total_dec_flops = 0.0
        self.total_pixel_count = 0
        self.resume = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'pretrained_models', 'resnet50-19c8e357.pth')
        self.model = models.resnet50()
        self.device= torch.device('cpu')
        if torch.cuda.is_available():
            self.device = torch.device('cuda:0')
        self.model = self.model.to(self.device)
                
    def build_dataset(self):
        normalize = transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        r = re.compile(self.file_name_r)
        dataset = CustomFolder(
                self._dataset_path,
                transforms.Compose([
                    transforms.Resize(256),
                    transforms.CenterCrop(256),
                    transforms.ToTensor(),
                    normalize,
                ]),
                is_valid_file=lambda x: r.match(x) is not None
            )
        collate_fn = None
        loader = torch.utils.data.DataLoader(
                dataset,
                batch_size=1,
                shuffle=False,
                num_workers=4,
                pin_memory=True,
                sampler=None,
                collate_fn=collate_fn
            )
        return loader

    def preprocess(self, img, target):
        img = img.to(self.device)
        target = target.to(self.device)
        return img, target

    def accuracy(self, output, target, topk=(1,)):
        """Computes the precision@k for the specified values of k"""
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res
    
    def perform_metrics(self):
        from ptflops.flops_counter import flops_to_string
        ans = ' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'.format(top1=self.top1, top5=self.top5)
        ans += f'\nTotal pixels {self.total_pixel_count}'
        ans += f'\nClassification time {self.total_time_in_sec} sec, GPU decoding time {self.total_decGPU_time_in_sec}, CPU decoding time {self.total_decCPU_time_in_sec}, total time: {self.total_decCPU_time_in_sec + self.total_decGPU_time_in_sec + self.total_time_in_sec}'
        ans += f'\nClassification flops {self.total_flops} Mac, decoding flops {self.total_dec_flops} Mac, total flops {self.total_flops + self.total_dec_flops} Mac'
        m = 1E-3 / (self.total_pixel_count)
        ans += f'\nClassification flops {self.total_flops  * m} KMac/px, decoding flops {self.total_dec_flops  * m} KMac/px, total flops {(self.total_flops + self.total_dec_flops) * m} KMac/px'
        #total pixels {self.total_pixel_count}, {flops_to_string(self.total_flops/self.total_pixel_count, units="KMac")} / px'
        print(ans)
        with open(os.path.join(self.output_dir, "Classification_summary.txt"), "w") as f:
            f.write(ans)
    
    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument(f"--{self.name}.files_mask", type=str, default=".*\.(JPEG|jpeg)", help="RegEx mask for name of files to be processed")
        parser.add_argument(
                f'--{self.name}.data_dir',
                default=os.path.join(os.getcwd(), "dataset_classification"),
                type=str,
                help='path to dataset'
            )
        parser.add_argument("--output", type=str, default=self.output_dir, help="Path to base output directory")
    
    def parse_arguments(self, parser: argparse.ArgumentParser):
        p = vars(parser.parse_args())
        self.output_dir = p['output']
        self.file_name_r = re.compile(p[f"{self.name}.files_mask"])
        self._dataset_path = p[f'{self.name}.data_dir']
    
    def download_dataset(self):
        ans = True
        if not os.path.exists(self._dataset_path):
            print(f"Please manualy download dataset to folder {self._dataset_path}")
            ans = False
        if ans:
            self.loader = self.build_dataset()
        return ans
    
    def download_weights(self):
        ans = True
        if not os.path.exists(self.resume):
            os.makedirs(os.path.dirname(self.resume), exist_ok=True)
            ans = self.download("https://download.pytorch.org/models/resnet50-19c8e357.pth", self.resume)
            if not ans:
                print(f"Please manualy download checkpoint to {self.resume}")
                exit(-104)
        print("=> loading checkpoint '{}'".format(self.resume))
        checkpoint = torch.load(self.resume, map_location=self.device)
        if "state_dict" in checkpoint:
            self.model.load_state_dict(checkpoint['state_dict'])
        else:
            checkpoint_module = dict()
            for key in checkpoint.keys():
                checkpoint_module[key] = checkpoint[key]
            self.model.load_state_dict(checkpoint_module)

        return ans


    @staticmethod
    def load_complexity_info(rec_path):
        """
        Load additional information

        Args:
            rec_path (str): path to reconstructed file
        Return:
            kmac (float): complexity in kMAC per pixel
            decGPU (float): decoder complexity on GPU in sec.
            decCPU (float): decoder complexity on CPU in sec.
            encGPU (float): encoder complexity on GPU in sec.
            encCPU (float): encoder complexity on CPU in sec.
        """
        import json
        r_fn, _ = os.path.splitext(rec_path)
        in_path = f'{r_fn}.json'
        ans = {
            'kmac': None,
            'decGPU': None,
            'decCPU': None,
            'encGPU': None,
            'encCPU': None
        }
        if os.path.exists(in_path):
            with open(in_path, 'r') as f:
                ans = json.load(f)
        return ans['kmac'], ans['decGPU'], ans['decCPU'], ans['encGPU'], ans[
            'encCPU']

    @staticmethod
    def init_ptflops_calc(model):
        import sys

        from ptflops.flops_counter import add_flops_counting_methods
        ans = add_flops_counting_methods(model)
        ans.start_flops_count(ost=sys.stdout, verbose=False, ignore_list=[])
        return ans

    @staticmethod
    def finish_ptflops_calc(model, size=None):
        """
        Get flops of the model

        Args:
            model (nn.Module): model, which we examine
            size (list, optional): shape of the input image (width, height). Defaults to None.

        Returns:
            float: MACs
            float: MACs per pixel
        """
        # Flops calculation
        flops_count, _ = model.compute_average_flops_cost()
        flops_per_pixel = None
        if size is not None:
            flops_per_pixel = flops_count / (size[0] * size[1])
        return flops_count, flops_per_pixel


    def run(self):
        from datetime import datetime
        import ptflops
        self.prerun_hook()
        self.model.eval()
        self.top1.reset()
        self.top5.reset()
        self.model = self.init_ptflops_calc(self.model)
        with torch.no_grad():
            start_time = datetime.now()
            for i, (data, target, path) in enumerate(self.loader):
                self.total_pixel_count += data.shape[-1] * data.shape[-2]
                torch.cuda.empty_cache()
                data, target = self.preprocess(data, target)
                output = self.model(data)
                acc1, acc5 = self.accuracy(output.data, target, topk=(1, 5))
                _, top1_id = output.data.topk(1, 1, True, True)
                top1_label = self.loader.dataset.idx_to_class[int(top1_id.detach().cpu().squeeze().numpy())]
                target_label = os.path.basename(os.path.dirname(path[0]))
                kmacPpx, decGPU, decCPU, _, _ = self.load_complexity_info(path[0])
                if kmacPpx is not None:
                    self.total_dec_flops += 1000 * kmacPpx * data.shape[-1] * data.shape[-2]
                if decGPU is not None:
                    self.total_decGPU_time_in_sec += decGPU
                if decCPU is not None:
                    self.total_decCPU_time_in_sec += decCPU
                print(
                    top1_label==target_label,
                    top1_label,
                    target_label,
                    path[0],
                )
                self.top1.update(acc1[0], data.size(0))
                self.top5.update(acc5[0], data.size(0))
            total_time = datetime.now() - start_time
            self.total_time_in_sec = total_time.total_seconds()
            self.total_flops, _  = self.finish_ptflops_calc(self.model)

if __name__ == "__main__":
    from common.Base import make_main_func
    from . import get_models
    model = get_models()
    make_main_func(model)
