# Classification JPEG-AI

The repository for image classification task in JPEG-AI framework.

The scripts realized possibility to enerate two types of anchors:
1. Original anchor
2. Decoded anchor

## Usage<a name="Usage"></a>

The ResNet50 pretrained model is adopted as classification anchor model.
To run the validation, the scripts should download the pretrained ResNet50 model automatically from [url](https://download.pytorch.org/models/resnet50-19c8e357.pth). If it doesn't happen you may download it manually and put it to a directory `<ANCHORS_SCRIPS_PATH>/Classification/pretrained_models`.

The following example runs validation for ResNet50 on the given dataset:
```python
python -m Classification.process --Classification.data_dir /path/to/dataset --output /path/to/output_dir
```

# Anchors

## Original anchor

To evaluate the anchor as Original anchor download ImageNet dataset (as described below) anduse command line from [`Usage`](#Usage) section, where `/path/to/dataset` is a path to ImageNet dataset.

### ImageNet Dataset<a name="Dataset"></a>
On the validation set of [ILSVRC 2012](https://www.image-net.org/challenges/LSVRC/2012/index.php), top one and five accuracy should be 76.72% and 93.34%.
Download the set to folder `<ANCHORS_SCRIPS_PATH>/dataset_classification`.

Structure of the directory:

    DATASET_DIRECTORY
        n01440764
            ILSVRC2012_val_00000293.JPEG
            ILSVRC2012_val_00002138.JPEG
            ...

        n01443537
            ILSVRC2012_val_00000236.JPEG
            ILSVRC2012_val_00000262.JPEG
            ...

        ...


## Decoded anchor

Generation of decoded anchor performs based on images decoded by some third party codec. You may put reconstructed images to a directory with the same structure as [ImageNet dataset](#Dataset) and run the scripts as it describes in [`Usage`](#Usage) section, where `/path/to/dataset` is a path to the directory with decoded images. 
