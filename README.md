# JPEG-AI Anchors

JPEG-AI Anchor generators

# How to set-up

1. clone repo by command: `git clone https://gitlab.com/wg1/jpeg-ai/jpeg-ai-anchors.git` 
2. update submodules: `git submodule update --init --recursive`
4. create conda environment: ``conda create -n jpeg_ai_anchors python=3.6.7``
5. activate environment: ``conda activate jpeg_ai_anchors``
6. Upgrade `pip` by a command `python -m pip install --upgrade pip`
7. install dependencies: ``find . -name "requirement*" -type f -exec pip3 install -r '{}' ';'``
8. download VMAF binary from [here](https://github.com/Netflix/vmaf/releases/download/v2.1.1/vmaf.linux) to `metrics` and add executable bit by command `chmod +x vmaf.linux`
9. Install the following packages:
    - FFMPEG 3.4.8
    - Image Magick 6.9.7-4 Q16 20170114
    - CMake 3.10.2 (or above)
    - Autoconf 2.69-11 (or above). You may use command `apt-get install autoconf=2.69-11` to install it in Ubuntu.


# List of data for downloading

List of data, which should be downloaded manually:

1. Dataset for compression task [sftp://jpeg-ai@amalia.img.lx.it.pt](sftp://jpeg-ai@amalia.img.lx.it.pt) (password to be given by request) should be downloaded to directory `dataset`.
2. Download validation part of [ImageNet dataset (ILSVRC 2012)](https://www.image-net.org/challenges/LSVRC/2012/index.php) to directory `dataset_classification`.
3. Download JPEG2000 binaries from [here](https://kakadusoftware.com/documentation-downloads/downloads/) and uncompress to `Compression/JPEG2000/src/KDU805_Demo_Apps_for_Linux-x86-64_200602`

Source code, which the scripts will try to download automatically:

1. Download JPEG source code from [here](https://jpeg.org/downloads/jpegxt/reference1367abcd89.zip) and uncompress to `Compression/JPEG/src/jpeg_src`
2. Download HEVC source code from [here](https://hevc.hhi.fraunhofer.de/svn/svn_HEVCSoftware/tags/HM-16.20+SCM-8.8/) to `Compression/HEVC/src`.
3. Download VVC source code from [here](https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM/-/archive/VTM-11.1/VVCSoftware_VTM-VTM-11.1.tar) and uncompress to `Compression/VVC/src/VVCSoftware_VTM-VTM-11.1`.
4. Download weights for classification task from [here](https://download.pytorch.org/models/resnet50-19c8e357.pth) to directory `Classification/pretrained_models`.

# Usage

Use command:
```
python -m process
```

Scripts will create directory `Anchors` with summary files for each task. The summary files will have the following notation:
```
<TASK_OR_CODEC_NAME>_summary.txt
```

where `<TASK_OR_CODEC_NAME>` may have the following values: Classification, JPEG, JPEG2000, HEVC, VVC.

# Tasks
1. [Classification](Classification/README.md)
2. [Compression](Compression/README.md)
3. [Denoising](Denoising/README.md)
4. [Super Resolution](SuperResolution/README.md)