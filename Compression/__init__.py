
def get_models():
    from .JPEG.process import JPEGCodec
    #from .JPEGXL.process import JPEGXLCodec
    from .JPEG2000.process import JPEG2000Codec
    from .HEVC.process import HEVCCodec
    from .VVC.process import VVCCodec 
    
    return [JPEGCodec(), JPEG2000Codec(), HEVCCodec(), VVCCodec()] # , JPEGXLCodec()