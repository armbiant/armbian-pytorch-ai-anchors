import os
import re

r = re.compile("(?P<qp>\d+)_(?P<name>.*)_(?P<w>\d+)x(?P<h>\d+)")

with open("output.txt", "wt") as f:
    for fn in os.listdir():
        if fn.endswith(".bits"):
            s = r.search(fn)
            size = os.path.getsize(fn)
            bpp =8*size / (int(s.group("w")) * int(s.group("h")))
            f.write(f"{fn}\t{s.group('qp')}\t{s.group('name')}\t{s.group('w')}\t{s.group('h')}\t{size}\t{bpp}\n")
        
     