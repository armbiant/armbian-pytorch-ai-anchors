# Compression anchors for JPEG-AI

Scripts for anchor generation for compression task

# Requirements

- FFMPEG 3.4.8
- Image Magick 6.9.7-4 Q16 20170114
- CMake 3.10.2 (or above)


# Dataset

The first 16 images from test dataset of JPEG-AI CfE are used for evaluation. Download them manually from [sftp://jpeg-ai@amalia.img.lx.it.pt](sftp://jpeg-ai@amalia.img.lx.it.pt) (password to be given by request) and put to directory `<ANCHORS_SCRIPS_PATH>/dataset`. 

# Anchors

Compression anchors includes:
- JPEG  (ISO/IEC 10918-1 | ITU-T Rec. T.81)
- JPEG 2000  (ISO/IEC 15444-1 | ITU-T Rec. T.800)
- HEVC Intra (ISO/IEC 23008-2 | ITU-T Rec. H.265)
- VVC Intra (ISO/IEC 23090-3 | ITU-T Rec. H.266)

## Bitrate matching

For bitrate matching you need to perfrom the following steps:
1. Generate streams for some range of QP by command:

``
python -m Compression.<CODEC>.process --<CODEC>_list "" --<CODEC>_qp_start <QP_START> --<CODEC>_qp_end <QP_END> --output <OUTPUT_DIR>
``

where `<CODEC>` is a name of codec under test, `<QP_START>` is lowest QP in the range, `<QP_END>` is highes QP in the range, `<OUTPUT_DIR>` is temporal directory for output streams.

If you have a list with QP and want to iterate over it in some range, you may use the following command line:

``
python -m Compression.<CODEC>.process --<CODEC>_list <PATH_TO_LIST> --<CODEC>_qp_start <QP_START> --<CODEC>_qp_end <QP_END> --<CODEC>_qp_search --output <OUTPUT_DIR>
``

where `<PATH_TO_LIST>` is a path to file with list in format `<IMAGE_NAME> <TARGET_BPP_MUL_BY_100> <QP>`. Script will iterates in QP range from `<QP>+<QP_START>` to `<QP>+<QP_END>`.

2. Run bitrates matcher and generate list with QPs:

`` 
python Compression/bitrate_matcher.py <OUTPUT_DIR>/<CODEC>/bit Compression/<CODEC>/qps.txt
``

## JPEG

Scripts download and compile source of JPEG automatically. If you have any trouble with it, you may download source code manually from [here](https://github.com/thorfdbg/libjpeg/archive/966a8ed42f07bbb9ce20390e61e0d0601fb479c6.zip), uncompress all source files to `<ANCHORS_SCRIPS_PATH>/Compression/JPEG/src/libjpeg-966a8ed42f07bbb9ce20390e61e0d0601fb479c6` and build by command `make`.

### Dataset preparation

Dataset should be converted from `png` format to `pnm`. Software performs that automatically by command:
```
convert <ANCHORS_SCRIPS_PATH>/dataset/<FILE_NAME>.png -strip <ANCHORS_SCRIPS_PATH>/dataset_pnm/<FILE_NAME>.pnm
```

### Usage

Compression is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/JPEG/src/jpeg_src/jpeg -q <QP> -h -qt 3 -s 1x1,2x2,2x2 <ANCHORS_SCRIPS_PATH>/dataset_pnm/<FILE_NAME>.pnm <OUTPUT>/JPEG/bit/<BIT_NAME>.bits
```

Reconstruction is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/JPEG/src/jpeg_src/jpeg <OUTPUT>/JPEG/bit/<BIT_NAME>.bits <OUTPUT>/JPEG/rec/<REC_NAME>.pnm
```

Backward conversion to PNG:
```
convert <OUTPUT>/JPEG/rec/<REC_NAME>.pnm <OUTPUT>/JPEG/rec/<REC_NAME>.png
```


## JPEG 2000

Binaries should be manually downloaded from [here](https://kakadusoftware.com/documentation-downloads/downloads/), uncompress it to `<ANCHORS_SCRIPS_PATH>/Compression/JPEG2000/src/KDU805_Demo_Apps_for_Linux-x86-64_200602`. Desirable version of the software is `v8.0.5`.

### Dataset preparation

Dataset should be converted from `png` format to `ppm`. Software performs that automatically by command:
```
convert <ANCHORS_SCRIPS_PATH>/dataset/<FILE_NAME>.png -strip <ANCHORS_SCRIPS_PATH>/dataset_pnm/<FILE_NAME>.ppm
```

### Usage

Compression with MSE weighting is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/JPEG2000/src/KDU805_Demo_Apps_for_Linux-x86-64_200602/kdu_compress -i <ANCHORS_SCRIPS_PATH>/dataset_pnm/<FILE_NAME>.ppm -o <OUTPUT>/JPEG2000/bit/<BIT_NAME>.bits -rate <TARGET_BPP> Qstep=0.001 -tolerance 0 -full -precise -no_weights -num_threads 1
```

Compression with Visually weighting is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/JPEG2000/src/KDU805_Demo_Apps_for_Linux-x86-64_200602/kdu_compress -i <ANCHORS_SCRIPS_PATH>/dataset_pnm/<FILE_NAME>.ppm -o <OUTPUT>/JPEG2000/bit/<BIT_NAME>.bits -rate <TARGET_BPP> Qstep=0.001 -tolerance 0 -full -precise -num_threads 1
```

Reconstruction is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/JPEG2000/src/KDU805_Demo_Apps_for_Linux-x86-64_200602/kdu_expand -i <OUTPUT>/JPEG2000/bit/<BIT_NAME>.bits -o <OUTPUT>/JPEG2000/rec/<REC_NAME>.ppm -precise -num_threads 1
```

Backward conversion to PNG:
```
convert <OUTPUT>/JPEG2000/rec/<REC_NAME>.ppm <OUTPUT>/JPEG2000/rec/<REC_NAME>.png
```


## HEVC

Scripts download and compile source of HEVC automatically. If you have any trouble with it, you may download source code manually from [https://hevc.hhi.fraunhofer.de/svn/svn_HEVCSoftware/tags/HM-16.20+SCM-8.8/] to `<ANCHORS_SCRIPS_PATH>/Compression/HEVC/src` and compile them by command `make`.

### Dataset preparation

Dataset should be converted from `png` format to `yuv`. Software performs that automatically by command:
```
ffmpeg -hide_banner -i <ANCHORS_SCRIPS_PATH>/dataset/<FILE_NAME>.png -pix_fmt yuv444p10le -vf scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709 -color_primaries bt709 -color_trc bt709 -colorspace bt709 -y <ANCHORS_SCRIPS_PATH>/dataset_yuv/<FILE_NAME>.yuv
```

### Usage

Compression is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/HEVC/src/bin/TAppEncoderStatic -c <ANCHORS_SCRIPS_PATH>/Compression/HEVC/encoder_intra_main_scc_10.cfg -i <ANCHORS_SCRIPS_PATH>/dataset_yuv/<FILE_NAME>.yuv -wdt <WIDTH> -hgt <HEIGHT> -b <OUTPUT>/HEVC/bit/<BIT_NAME>.bits -f 1 -fr 25 -q <QP> --FrameSkip=0 --InputBitDepth=10 --InputChromaFormat=444 --ChromaFormatIDC=444 --Level=6.2
```

Reconstruction is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/HEVC/src/bin/TAppDecoderStatic -d 10 -b <OUTPUT>/HEVC/bit/<BIT_NAME>.bits -r <OUTPUT>/HEVC/rec/<REC_NAME>.yuv
```

Backward conversion to PNG:
```
ffmpeg -f rawvideo -vcodec rawvideo -s <WIDTH>x<HEIGHT> -r 25 -pix_fmt yuv444p10le -i <OUTPUT>/HEVC/rec/<REC_NAME>.yuv -pix_fmt rgb24 -vf scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709 -color_primaries bt709 -color_trc bt709 -colorspace bt709 -y <OUTPUT>/HEVC/rec/<REC_NAME>.png
```


## VVC

Scripts download and compile source of HEVC automatically. If you have any trouble with it, you may download source code manually from [here](https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM/-/archive/VTM-11.1/VVCSoftware_VTM-VTM-11.1.tar) and uncompress to `<ANCHORS_SCRIPS_PATH>/Compression/VVC/src/VVCSoftware_VTM-VTM-11.1` and compile them by commands `cmake . && make`.

There are two files with QPs in `Compression/VVC` directory. The default one (qps.txt) includes only reduced list of rate points (6, 12, 25, 50, 75). The second file (qps_full.txt) includes all rate points.

You can set which QPs file would you like to use:
```
python -m Compression.VVC.process  --VVC_list <PATH_TO_QPS_FILE>
```

where `<PATH_TO_QPS_FILE>` is a path to QPs file.



### Dataset preparation

Dataset should be converted from `png` format to `yuv`. Software performs that automatically by command:
```
ffmpeg -hide_banner -i <ANCHORS_SCRIPS_PATH>/dataset/<FILE_NAME>.png -pix_fmt yuv444p10le -vf scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709 -color_primaries bt709 -color_trc bt709 -colorspace bt709 -y <ANCHORS_SCRIPS_PATH>/dataset_yuv/<FILE_NAME>.yuv
```

### Usage

Compression is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/VVC/src/VVCSoftware_VTM-VTM-11.1/bin/EncoderAppStatic -c <ANCHORS_SCRIPS_PATH>/Compression/VVC/src/VVCSoftware_VTM-VTM-11.1/cfg/encoder_intra_vtm.cfg -i <ANCHORS_SCRIPS_PATH>/dataset_yuv/<FILE_NAME>.yuv -wdt <WIDTH> -hgt <HEIGHT> -b <OUTPUT>/VVC/bit/<BIT_NAME>.bits -f 1 -fr 10 -q <QP> --InputBitDepth=10 --InputChromaFormat=444 --ChromaFormatIDC=444 --TemporalSubsampleRatio=1 --Level=6.2 --ConformanceWindowMode=1
```

Reconstruction is executed by command:
```
<ANCHORS_SCRIPS_PATH>/Compression/VVC/src/VVCSoftware_VTM-VTM-11.1/bin/DecoderAppStatic -d 10 -b <OUTPUT>/VVC/bit/<BIT_NAME>.bits -o <OUTPUT>/VVC/rec/<REC_NAME>.yuv
```

Backward conversion to PNG:
```
ffmpeg -f rawvideo -vcodec rawvideo -s <WIDTH>x<HEIGHT> -r 25 -pix_fmt yuv444p10le -i <OUTPUT>/VVC/rec/<REC_NAME>.yuv -pix_fmt rgb24 -vf scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709 -color_primaries bt709 -color_trc bt709 -colorspace bt709 -y <OUTPUT>/VVC/rec/<REC_NAME>.png
```
