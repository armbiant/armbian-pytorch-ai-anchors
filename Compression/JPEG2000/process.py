from ..process import CodecParent
import os
import subprocess
import re
import argparse

class JPEG2000Codec(CodecParent):
    def __init__(self, *args, **kwards):
        super(JPEG2000Codec, self).__init__("JPEG2000", "ppm", *args, **kwards)
        self.src_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "src")
        self.jpeg2000_path = os.path.join(self.src_path, "KDU805_Demo_Apps_for_Linux-x86-64_200602")
        self.enc_path = os.path.join(self.jpeg2000_path, "kdu_compress")
        self.dec_path = os.path.join(self.jpeg2000_path, "kdu_expand")
        self.cfg_path = self.jpeg2000_path
        self._r = re.compile("(?P<name>.+)_(?P<width>\d+)x(?P<height>\d+)")
        self._set_enc_dec_func(self.jpeg2000_enc_dec)
        
    @staticmethod
    def jpeg2000_enc_dec(cfg):
        import multiprocessing
        enc_path = cfg['enc_path']
        dec_path = cfg['dec_path']
        cfg_path = cfg['cfg_path']
        #width = cfg['width']
        #height = cfg['height']
        qp = cfg['qp']
        f = cfg['f']
        val_conv_path = cfg['val_conv_path']
        bit_fn = cfg['bit_fn']
        rec_fn = cfg['rec_fn']
        png_fn = cfg['png_fn']
        gen_rec = cfg['gen_rec']
        enc_t, dec_t = -1, -1
        name = cfg['codec_name']
        log_func = cfg['log_fn']

        e_lib_path = os.environ.get("LD_LIBRARY_PATH")
        e_path = os.environ.get("PATH")
        env_val = {
                                  "PATH": f"{e_path}:{cfg_path}",
                                  "LD_LIBRARY_PATH": f"{e_lib_path}:{cfg_path}" if e_lib_path is not None else cfg_path
                              }

        # Encode MSE variant
        log_func(name, f"Encode MSE variant. Process {multiprocessing.current_process().name}")
        cmd = [enc_path,"-i", os.path.join(val_conv_path, f),
                                        "-o", bit_fn,
                                        "-rate", str(qp / 100),
                                        "Qstep=0.001",
                                        "-tolerance", "0",
                                        "-full",
                                        "-precise",
                                        "-no_weights", "-num_threads", "1"
                                        ]
        ans, enc_t = CodecParent._call_with_time_measurment(cmd, env=env_val)
        
        if ans != 0:
            print(f"Cannot encode MSE variant for {f}")
            exit(-12)
            
        rec_dir = os.path.dirname(rec_fn)
        if gen_rec:
            bit_dir_mse = os.path.dirname(bit_fn)
            bit_dir_vis = bit_dir_mse + "_vis"
            bit_fn_basename = os.path.basename(bit_fn)
            bit_vis_fn = os.path.join(bit_dir_vis, bit_fn_basename)
            os.makedirs(bit_dir_vis, exist_ok=True)
            
            # Encode Visual variant
            log_func(name, f"Encode Visual variant. Process {multiprocessing.current_process().name}")
            cmd = [enc_path,"-i", os.path.join(val_conv_path, f),
                                            "-o", bit_vis_fn,
                                            "-rate", str(qp / 100),
                                            "Qstep=0.001",
                                            "-tolerance", "0",
                                            "-full",
                                            "-precise",
                                            "-num_threads", "1"
                                            ]
            ans, enc_t = CodecParent._call_with_time_measurment(cmd, env=env_val)
            
            if ans != 0:
                print(f"Cannot encode Visual variant for {f}")
                exit(-12)

            # Decode MSE variant
            cmd = [dec_path, "-i", bit_fn,
                                                "-o", rec_fn,
                                                "-precise", "-num_threads", "1"]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd, env = env_val)
            if ans != 0:
                print(f"Cannot decode MSE variant {bit_fn}")
                exit(-13)
                
            # Convert to PNG
            CodecParent._convert_formats(rec_fn, png_fn)
            
            # Decode Visual variant
            log_func(name, f"Decode Visual variant. Process {multiprocessing.current_process().name}")
            rec_dir_vis = rec_dir + "_vis"
            rec_fn_vis = os.path.join(rec_dir_vis, os.path.basename(rec_fn))
            os.makedirs(rec_dir_vis, exist_ok=True)
            cmd = [dec_path, "-i", bit_vis_fn,
                                                "-o", rec_fn_vis,
                                                "-precise", "-num_threads", "1"]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd, env = env_val)
            if ans != 0:
                print(f"Cannot decode MSE variant {bit_fn}")
                exit(-13)
                
            # Convert to PNG
            log_func(name, f"Convert Visual variant to PNG. Process {multiprocessing.current_process().name}")
            png_fn_vis = os.path.join(rec_dir_vis, os.path.basename(png_fn))
            _, conv_t = CodecParent._convert_formats(rec_fn_vis, png_fn_vis)

            log_func(name, f"Write logs. Process {multiprocessing.current_process().name}")
            from metrics.metrics import MetricsProcessor
            MetricsProcessor.store_complexity_info(png_fn, encCPU=enc_t, encGPU=enc_t, decCPU=dec_t+conv_t, decGPU=dec_t+conv_t)
            log_func(name, f"Finished process {multiprocessing.current_process().name}")
        return 0
    
    def parse_arguments(self, parser: argparse.ArgumentParser):
        super().parse_arguments(parser)
        self.max_threads = 1        # Set to 1 thread, because processes hangs after execution

    def download_code(self):
        ans = True
        # Download source ocde of VTM
        if not os.path.exists(self.jpeg2000_path):
            print(f"Please download manually KDU805_Demo_Apps_for_Linux-x86-64_200602.zip (for Linux) from https://kakadusoftware.com/documentation-downloads/downloads/ and unzip to folder {self.jpeg2000_path}")
            exit(-101)
            
        # TODO: add unzipping
                    
        return ans                

    def compile_code(self):
        return True

    
if __name__ == "__main__":
    from common.Base import make_main_func
    obj = JPEG2000Codec()
    make_main_func(obj)