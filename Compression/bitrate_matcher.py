import os
import re
import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument("input_dir", default="", help="Path to directory with bitstreams")
    parser.add_argument("output_file", default="", help="Path to output file with QPS")
    parser.add_argument("--target-bpp", type=int, nargs="+", default=[200, 150, 100, 75, 50, 25, 12, 6, 3], help="Target bpp multiplied by 100")

    args = parser.parse_args()

    r = re.compile("(?P<qp>\d+)_(?P<name>.*)_(?P<w>\d+)x(?P<h>\d+)")
    r_seq_name = re.compile("(\d+)_(?P<seq_name>.+)\.bits")

    BPP_dict = {}

    for fn in os.listdir(args.input_dir):
        if fn.endswith(".bits"):
            s = r.search(fn)
            size = os.path.getsize(os.path.join(args.input_dir, fn))
            bpp =8*size / (int(s.group("w")) * int(s.group("h")))
            #f.write(f"{fn}\t{s.group('qp')}\t{s.group('name')}\t{s.group('w')}\t{s.group('h')}\t{size}\t{bpp}\n")
            #seq_name = f"{s.group('name')}_{s.group('w')}x{s.group('h')}"
            sn = r_seq_name.search(fn)
            seq_name = sn.group("seq_name")
            if seq_name in BPP_dict:
                BPP_dict[seq_name][int(s.group('qp'))] = bpp
            else:
                BPP_dict[seq_name] = {int(s.group('qp')): bpp}
                
    with open(args.output_file, "w") as f:
        for k in sorted(list(BPP_dict.keys())):
            cur_seq_params = BPP_dict[k]
            for tbpp in args.target_bpp:
                tbpp_f = float(tbpp) / 100.0
                best_qp = max(cur_seq_params.keys())
                best_diff = cur_seq_params[best_qp] - tbpp_f
                for qp,bpp in cur_seq_params.items():
                    if bpp < cur_seq_params[best_qp]:
                        best_qp = qp
                        best_diff = bpp - tbpp_f
                for qp in cur_seq_params.keys():
                    cur_diff = cur_seq_params[qp] - tbpp_f
                    if cur_diff <= 0.1 * tbpp_f and (best_diff >  0.1 * tbpp_f or cur_diff > best_diff):
                        best_diff = cur_diff
                        best_qp = qp
                f.write(f"{k}\t{tbpp}\t{best_qp}\n")
            
     