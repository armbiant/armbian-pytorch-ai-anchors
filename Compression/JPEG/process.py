from ..process import CodecParent
import os
import subprocess
import re
import argparse

class JPEGCodec(CodecParent):
    def __init__(self, *args, **kwards):
        super(JPEGCodec, self).__init__("JPEG", "pnm", *args, **kwards)
        self.src_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "src")
        self.jpeg_path = os.path.join(self.src_path, "libjpeg-966a8ed42f07bbb9ce20390e61e0d0601fb479c6")
        self.enc_path = os.path.join(self.jpeg_path, "jpeg")
        self.dec_path = os.path.join(self.jpeg_path, "jpeg")
        self.cfg_path = self.jpeg_path
        self._r = re.compile("(?P<name>.+)_(?P<width>\d+)x(?P<height>\d+)")
        self._set_enc_dec_func(self.jpeg_enc_dec)
        
    @staticmethod
    def jpeg_enc_dec(cfg):
        import subprocess
        enc_path = cfg['enc_path']
        dec_path = cfg['dec_path']
        cfg_path = cfg['cfg_path']
        width = cfg['width']
        height = cfg['height']
        qp = cfg['qp']
        f = cfg['f']
        val_conv_path = cfg['val_conv_path']
        bit_fn = cfg['bit_fn']
        rec_fn = cfg['rec_fn']
        png_fn = cfg['png_fn']
        gen_rec = cfg['gen_rec']
        enc_t, dec_t = -1, -1

        # Encode
        cmd = [enc_path, "-h", "-qt", "3", "-s", "1x1,2x2,2x2", "-q", str(qp),
                                        os.path.join(val_conv_path, f),
                                        bit_fn]
        ans, enc_t = CodecParent._call_with_time_measurment(cmd)
        if ans != 0:
            print(f"Cannot encode {f}")
            exit(-12)
            
        if gen_rec:
            # Decode
            cmd = [dec_path, bit_fn, rec_fn]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd)
            if ans != 0:
                print(f"Cannot decode {bit_fn}")
                exit(-13)
                
            # Convert to PNG
            _, conv_t = CodecParent._convert_formats(rec_fn, png_fn)
                    
            from metrics.metrics import MetricsProcessor
            MetricsProcessor.store_complexity_info(png_fn, encCPU=enc_t, encGPU=enc_t, decCPU=dec_t+conv_t, decGPU=dec_t+conv_t)


    def download_code(self):
        ans = True
        zip_path = os.path.join(self.src_path, "src.zip")
        # Download source ocde of VTM
        if not os.path.exists(self.jpeg_path):
            os.makedirs(self.jpeg_path)
            ans = self.download("https://github.com/thorfdbg/libjpeg/archive/966a8ed42f07bbb9ce20390e61e0d0601fb479c6.zip", zip_path)
        
        #import zipfile
        #with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        #    zip_ref.extractall(self.jpeg_path)
        import shutil
        shutil.unpack_archive(zip_path, self.src_path)
                    
        return ans                

    def compile_code(self):
        if not os.path.exists(self.enc_path) or not os.path.exists(self.dec_path):
            subprocess.call(['chmod', '-R', '0777', self.jpeg_path])
            ans = subprocess.call(["make"], cwd=self.jpeg_path, shell=True) == 0
            #ans = os.system(f"cd {self.jpeg_path} && make")
            return ans 
        else:
            return True
        

    
if __name__ == "__main__":
    from common.Base import make_main_func
    obj = JPEGCodec()
    make_main_func(obj)